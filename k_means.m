function [means,idx]=k_means(x,c,iter,pos)

[~,d]=size(x);
means=rand_dir(c,d,pos);

xi2=sum(x.^2,2);
for i=1:iter
    mut2=sum(means.^2,2);
    dist=bsxfun(@plus,bsxfun(@plus,-2*x*means',xi2),mut2'); %get distance matrix
    [~,idx] = min(dist,[],2);%find points closest to mean vector
    u=unique(idx);%number of clusters n has been assigned to incase some clusters are
    %dropped
    %using those indices create new mean vector
    new_mean=[];
    for j=1:length(u)
        sub_x=x(idx==u(j),:);
        mean_x=mean(sub_x,1);
        new_mean=[new_mean; mean_x];
    end
    means=new_mean;
end
